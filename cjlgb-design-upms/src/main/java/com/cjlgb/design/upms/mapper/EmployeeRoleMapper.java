package com.cjlgb.design.upms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cjlgb.design.common.upms.entity.EmployeeRole;

/**
 * @author WFT
 * @date 2020/7/14
 * description:
 */
public interface EmployeeRoleMapper extends BaseMapper<EmployeeRole> {


}
