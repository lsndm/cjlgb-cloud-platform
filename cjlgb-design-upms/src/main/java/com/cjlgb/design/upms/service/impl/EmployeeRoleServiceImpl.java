package com.cjlgb.design.upms.service.impl;

import com.cjlgb.design.common.mybatis.service.impl.BaseServiceImpl;
import com.cjlgb.design.common.upms.entity.EmployeeRole;
import com.cjlgb.design.upms.mapper.EmployeeRoleMapper;
import com.cjlgb.design.upms.service.EmployeeRoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @author WFT
 * @date 2020/7/14
 * description:
 */
@Service
@RequiredArgsConstructor
public class EmployeeRoleServiceImpl extends BaseServiceImpl<EmployeeRoleMapper, EmployeeRole> implements EmployeeRoleService {


}
